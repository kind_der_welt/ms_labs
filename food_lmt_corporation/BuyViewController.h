//
//  BuyViewController.h
//  food_lmt_corporation
//
//  Created by Student on 12.03.16.
//  Copyright © 2016 Student. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Food.h"

@interface BuyViewController : UIViewController

@property Food* product;

@end
