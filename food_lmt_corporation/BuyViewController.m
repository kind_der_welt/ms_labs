//
//  BuyViewController.m
//  food_lmt_corporation
//
//  Created by Student on 12.03.16.
//  Copyright © 2016 Student. All rights reserved.
//

#import "BuyViewController.h"

#import "Food.h"

#import <Quickblox/Quickblox.h>

@interface BuyViewController ()

@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UITextField *address;


@property (weak, nonatomic) IBOutlet UIStepper *stepper;
@property (weak, nonatomic) IBOutlet UILabel *amount;

@end

@implementation BuyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)stepper_value_changed:(id)sender {
    
    self.amount.text = [NSString stringWithFormat:@"%i", (int)([self.stepper value])];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)buysth:(id)sender {
    QBCOCustomObject *object = [QBCOCustomObject customObject];
    object.className = @"Order"; // your Class name
    
    // Object fields
    [object.fields setObject:self.name forKey:@"Name"];
    [object.fields setObject:self.phone forKey:@"Phone"];
    [object.fields setObject:self.address forKey:@"Address"];
    [object.fields setObject:@([self.stepper value]) forKey:@"Quantity"];
    [object.fields setObject:self.product.name forKey:@"Food_name"];
    
    [QBRequest createObject:object successBlock:^(QBResponse *response, QBCOCustomObject *object) {
        // do something when object is successfully created on a server
    } errorBlock:^(QBResponse *response) {
        // error handling
        NSLog(@"Response error: %@", [response.error description]);
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
