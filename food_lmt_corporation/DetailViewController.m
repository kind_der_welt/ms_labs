//
//  DetailViewController.m
//  food_lmt_corporation
//
//  Created by Student on 10.03.16.
//  Copyright © 2016 Student. All rights reserved.
//

#import "DetailViewController.h"
#import "Food.h"

#import "BuyViewController.h"


@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *price;


@end

@implementation DetailViewController

- (IBAction)Buy:(id)sender {
    BuyViewController *bvc = [[BuyViewController alloc] initWithNibName:@"BuyViewController" bundle:nil];
    
    bvc.product = self.detailItem;
    
    [self.navigationController pushViewController:bvc animated:YES];
    
}

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        self.detailDescriptionLabel.text = [self.detailItem full_description];
        self.name.text = [self.detailItem name];
        self.image.image = [UIImage imageNamed:@"sushi"];
        self.price.text = [NSString stringWithFormat:@"$ %@", [self.detailItem price]];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
