//
//  Food.h
//  food_lmt_corporation
//
//  Created by Student on 12.03.16.
//  Copyright © 2016 Student. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Food : NSObject


@property (strong) NSString *name;
@property (strong) NSString *full_description;
@property (strong) NSString *image_url;
@property (strong) NSNumber *price;



@end
