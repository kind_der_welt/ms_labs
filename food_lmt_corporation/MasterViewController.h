//
//  MasterViewController.h
//  food_lmt_corporation
//
//  Created by Student on 10.03.16.
//  Copyright © 2016 Student. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDatabase.h"

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;

@property NSURL* dbPath;
@property  FMDatabase *db;

@end

// /Users/student/Desktop/food_lmt_corporation/food_lmt_corporation/FMDatabasePool.h