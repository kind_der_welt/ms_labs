//
//  MasterViewController.m
//  food_lmt_corporation
//
//  Created by Student on 10.03.16.
//  Copyright © 2016 Student. All rights reserved.
// asdfasdf
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "Food.h"

#import <Quickblox/Quickblox.h>
#import "FMDatabase.h"


#import "UIImageView+WebCache.h"

@interface MasterViewController ()

@property NSMutableArray *objects;
@end

@implementation MasterViewController


- (NSURL *)dbPath
{
    return [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] URLByAppendingPathComponent:@"data.plist"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.objects) {
        self.objects = [[NSMutableArray alloc] init];
        

        
        _db = [FMDatabase databaseWithPath:[[self dbPath] absoluteString]];
        [_db open];
        
        [_db executeUpdate:@"CREATE TABLE IF NOT EXISTS food (name TEXT, full_description TEXT, image_url TEXT, price DOUBLE  )"];
        
                }
    
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    /*
    
    Food *f = [Food new];
    f.name = @"pizza";
    f.full_description = @"super puper pizza";
    f.image_url = @"https://www.google.com.ua/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwiNmpTRqbvLAhWln3IKHVygCgwQjRwIAw&url=http%3A%2F%2Ftroppa-nl.com%2F&psig=AFQjCNH-rHXm3E2xHVT63mZo4kzYwXSNFw&ust=1457878195842746";
    f.price = @10.0;
    
    Food *f1 = [Food new];
    f1.name = @"sushi";
    f1.full_description = @"super puper sushi";
    f1.image_url = @"https://www.google.com.ua/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwiNmpTRqbvLAhWln3IKHVygCgwQjRwIAw&url=http%3A%2F%2Ftroppa-nl.com%2F&psig=AFQjCNH-rHXm3E2xHVT63mZo4kzYwXSNFw&ust=1457878195842746";
    f1.price = @15.0;
    
    Food *f2 = [Food new];
    f2.name = @"soup";
    f2.full_description = @"super puper soup";
    f2.image_url = @"https://www.google.com.ua/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwjs78e3zt7LAhVL63IKHY1VB_4QjRwIBw&url=http%3A%2F%2Fweknowyourdreamz.com%2Fsoup.html&psig=AFQjCNHNIhpdlKUi-Zc-Wn0fZ7HLzN7iug&ust=1459090663667605";
    f2.price = @8.0;

    [self.objects addObject:f];
    [self.objects addObject:f1];
    [self.objects addObject:f2];
     
     */
    
    // Отправить запрос к КБ на получение объектов типа Food
    //
    
    [QBRequest objectsWithClassName:@"Food" successBlock:^(QBResponse * _Nonnull response, NSArray * _Nullable objects) {
        // Для каждого элемента массива objects (его тип QBCOCustomObject) создать объект типа Food.
        
        for (QBCOCustomObject *o in objects) {
            Food *f = [Food new];
            f.name = o.fields[@"name"];
            f.full_description = o.fields[@"full_description"];
            f.image_url = o.fields[@"image_url"];
            f.price = o.fields[@"price"];
            
            // Затем добавить в массив self.objects

            [self.objects addObject:f];
            
            
            NSString *SQL = [NSString stringWithFormat:@"INSERT INTO food (name, full_description, image_url, price  ) VALUES ('%@', '%@', '%@', %@)",
                             f.name, f.full_description, f.image_url, f.price];
            [_db executeUpdate:SQL];

        }
        
        // Заставить таблицу обновить данные.
        [self.tableView reloadData];
        
        
    } errorBlock:^(QBResponse * _Nonnull response) {
        
        
        
        NSString *SQL = [NSString stringWithFormat:@"SELECT * FROM food"];
        
        FMResultSet *result = [_db executeQuery:SQL];
        NSMutableString *str = [NSMutableString string];
        
        while ([result next])
        {
            Food *f = [Food new];
            f.name= [result stringForColumnIndex:0] ;
            f.full_description=[result stringForColumnIndex:1];
            f.image_url=[result stringForColumnIndex:2];
            f.price=[result stringForColumnIndex:3];
            
            [self.objects addObject:f];
        }
        
        [self.tableView reloadData];

               //
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender {
    return;
    if (!self.objects) {
        self.objects = [[NSMutableArray alloc] init];
    }
    [self.objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Food *object = self.objects[indexPath.row];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailItem:object];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    Food *f = self.objects[indexPath.row];
    cell.textLabel.text = f.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"$ %@", f.price];
    //cell.imageView.image = [UIImage imageNamed:@"sushi"];
    
    NSURL *url = [NSURL URLWithString:f.image_url];
    [cell.imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder"]];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

@end
